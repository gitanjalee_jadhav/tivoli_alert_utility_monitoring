/*
 * TivoliAlertUtilityMonitor.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * Jun 11, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoliutilitymonitoring.controller;

import java.io.File;
import java.io.StringWriter;
import java.util.Timer;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.boi.tivoliutilitymonitoring.businessLogic.TivoliAlertBussinessLogic;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliAlertConstants;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliErrorConstants;
import com.boi.tivoliutilitymonitoring.common.exception.TivoliAlertException;
import com.boi.tivoliutilitymonitoring.common.util.TivoliAlertProperties;

/**
 * This class to monitor  Tivoli optimization java utility fail files folder for any file
 * If file present in fail file folder, then email will be sent to DEV team.
 */
public class TivoliAlertUtilityMonitor {
	private static TivoliAlertUtilityMonitor tiUtilityMonitoring = null;
	private TivoliAlertBussinessLogic tAlertBussinessLogic = null;
	private StringWriter lEmailTemplateSw = null;
	private static TivoliAlertProperties prop = null;
	public static Boolean cIsDebug = false;
	public static Timer utilityStatuslogWriter = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertUtilityMonitor.class);


	public TivoliAlertUtilityMonitor() {
		cIsDebug = cLogger.isDebugEnabled();
	}
	/**
	 * This main method is entry point to monitor  Tivoli optimization java utility fail files folder for any file
	 * If file present in fail file folder, then email will be sent to DEV team.
	 */
	public static void main(String[] args) {

		try {
			prop = TivoliAlertProperties.getInstance();
			PropertyConfigurator.configure(prop
					.getProperty(TivoliAlertConstants.tivolimonitoring_logj));
			if (cIsDebug){
				cLogger.debug("TivoliAlertUtilityMonitor main() method Start ::: ");
			}

			File directory=new File(prop.getProperty(TivoliAlertConstants.failloc));
			int count = 0;
			for (File file : directory.listFiles()) {
				if (file.isFile()) {
					count++;
				}
			}
			if(count > 0)
			{
				if (cIsDebug){
					cLogger.debug("TivoliAlertUtilityMonitor main() method :Failed file directory contains files count -> "+count);
				}
				tiUtilityMonitoring = new TivoliAlertUtilityMonitor();
				tiUtilityMonitoring.processTivoliAlertUtilityMonitor(count);
			}
			if (cIsDebug){
				cLogger.debug("TivoliAlertUtilityMonitor main() method end ::: ");
			}
		} catch (Exception e) {
			try {
				if (e.toString() != null) {
					cLogger.error(e.getMessage());
					throw new TivoliAlertException(
							TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYMONITOR,
							e.toString());
				}
			} catch (TivoliAlertException ex) {
				cLogger.error(ex.getMessage());
				cLogger.error(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYMONITOR
						+ ex.toString());
				cLogger.fatal(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYMONITOR
						+ ex.toString());
			}
		}
	}
	/**
	 * This method will build email template and send mail to Dev team with fail file folder file count if files exist.
	 */
	public void processTivoliAlertUtilityMonitor(int count)
			throws TivoliAlertException {
		boolean isEmailSent =false;
		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertUtilityMonitor processTivoliAlertUtilityMonitor() method Start ::: ");
			}

			tAlertBussinessLogic = new TivoliAlertBussinessLogic();
			String msgText = prop.getProperty(TivoliAlertConstants.failedFiles_email_msg) + count;
			String subject = prop.getProperty(TivoliAlertConstants.failedFiles_email_subject);
			lEmailTemplateSw = tAlertBussinessLogic
					.handleEmailTemplateEvent(msgText);

			isEmailSent = tAlertBussinessLogic
					.handleEmailSenderEvent(lEmailTemplateSw , subject);

		} catch (TivoliAlertException e) {
			cLogger.error("TivoliAlertException in TivoliAlertUtilityMonitor processTivoliAlertUtilityMonitor()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYMONITOR,
					e.toString());

		} catch (Exception e) {
			cLogger.error("Exception in TivoliAlertUtilityMonitor processTivoliAlertUtilityMonitor()::"+e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYMONITOR,
					e.toString());
		}
	}
}
