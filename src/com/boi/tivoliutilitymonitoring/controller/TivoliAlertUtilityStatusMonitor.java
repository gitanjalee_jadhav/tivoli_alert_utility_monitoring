/*
 * TivoliAlertUtilityStatusMonitor.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * Jun 11, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoliutilitymonitoring.controller;

import java.io.File;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import com.boi.tivoliutilitymonitoring.businessLogic.TivoliAlertBussinessLogic;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliAlertConstants;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliErrorConstants;
import com.boi.tivoliutilitymonitoring.common.exception.TivoliAlertException;
import com.boi.tivoliutilitymonitoring.common.util.TivoliAlertProperties;

/**
 * This class will be responsible for checking Tivoli optimization utility status by checking last modified time of file.
 * If tivoliUtilityLog=D://TivoliDeploy//Logs//TivoliLog.txt is not updated from long time,
 *  it may be due to utility down reason, in such case email will be sent to Dev team. 
 */
public class TivoliAlertUtilityStatusMonitor {
	private static File file = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertUtilityStatusMonitor.class);
	public static Boolean cIsDebug = false;
	private static TivoliAlertProperties prop =null;
	
	public TivoliAlertUtilityStatusMonitor() {
		cIsDebug = cLogger.isDebugEnabled();
	}

	/**
	 * This  method is main entry point for Tivoli optimization Java utility status monitoring activity.
	 * Last modified time of tivoliUtilityLog=D://TivoliDeploy//Logs//TivoliLog.txt is verified with current time
	 * If difference is more than tivoliUtilityLog=5 mins, then email will be sent to DEV team with reason as -> utility down to check and take further action.
	 */
	public static void main(String [] args) {
		try {
			
			prop = TivoliAlertProperties.getInstance();
			PropertyConfigurator.configure(prop
					.getProperty(TivoliAlertConstants.tivolimonitoring_logj));
			if (cIsDebug){
				cLogger.debug("TivoliAlertUtilityStatusMonitor main() start : " + new Date());
			}
			
			file = new File(prop.getProperty(TivoliAlertConstants.tivoliUtilityLog));
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
			TivoliAlertBussinessLogic tAlertBussinessLogic = new TivoliAlertBussinessLogic();
			boolean isemailSend = tAlertBussinessLogic.handleUtilityStatus(sdf.format(file.lastModified()));
			if(isemailSend)
			{
				String msgText = prop.getProperty(TivoliAlertConstants.utilityDown_email_msg);
				String subject = prop.getProperty(TivoliAlertConstants.utilityDown_email_msg_email_subject);
				StringWriter lEmailTemplateSw = tAlertBussinessLogic
						.handleEmailTemplateEvent(msgText);

				boolean isEmailSent = tAlertBussinessLogic
						.handleEmailSenderEvent(lEmailTemplateSw , subject);
			}
		}  catch (TivoliAlertException e) {
			cLogger.error(e);
			cLogger.error(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYSTATUSMONITOR
					+ e.toString());
			cLogger.fatal(TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTUTILITYSTATUSMONITOR
					+ e.toString());
		}
		if (cIsDebug){
			cLogger.debug("TivoliAlertUtilityStatusMonitor main() method End ::: ");
		}
	}
}
