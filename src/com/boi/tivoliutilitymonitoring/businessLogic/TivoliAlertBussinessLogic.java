package com.boi.tivoliutilitymonitoring.businessLogic;

/*
 * TivoliAlertBussinessLogic.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * Jun 6, 2018	 1.0          OFSSL				 Created
 */
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.boi.tivoliutilitymonitoring.common.constants.TivoliAlertConstants;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliErrorConstants;
import com.boi.tivoliutilitymonitoring.common.exception.TivoliAlertException;
import com.boi.tivoliutilitymonitoring.common.util.TivoliAlertProperties;
import com.boi.tivoliutilitymonitoring.common.util.TivoliEmailSenderUtility;
import com.boi.tivoliutilitymonitoring.common.util.TivoliTemplateBuilder;

/**
 * This buciness logic class  handles the activity to build Email template and send mail for Tivoli alert utility status monitoring application
 */
public class TivoliAlertBussinessLogic {

	private boolean emailprinted = false;
	private String to = null;
	private String cc = null;
	private String from = null;
	private static Logger cLogger = Logger.getLogger(TivoliAlertBussinessLogic.class);
	public Boolean cIsDebug = false;
	private TivoliAlertProperties prop =null; 

	public TivoliAlertBussinessLogic() {
		cIsDebug = cLogger.isDebugEnabled();
	}
	/**
	 * This method handles the activity to Build Email template for Tivoli alert utility status monitoring application
	 */
	public StringWriter handleEmailTemplateEvent(String msgText)
					throws TivoliAlertException {
		StringWriter emailTemplate = null;
		try {
			if (cIsDebug) {
				cLogger.debug("Alert's email template build in TivoliAlertBussinessLogic.handleTivoliEmailTemplateEvent() start::");
			}

			TivoliTemplateBuilder templateBuilder = new TivoliTemplateBuilder();
			emailTemplate = templateBuilder.buildTemplate(msgText);
			if (cIsDebug) {
				cLogger.debug("Alert's email template built successfully in TivoliAlertBussinessLogic.handleTivoliEmailTemplateEvent() End ::");
			}
		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSINESSLOGIC,
					e.toString());
		}
		if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessLogic handleEmailTemplateEvent() method End ::: ");
		}
		return emailTemplate;
	}

	/**
	 * This method handles the activity to send mail to recipients related to Tivoli Optimization utility.
	 */
	public boolean handleEmailSenderEvent(StringWriter pEmailTemplate , String subject)
			throws TivoliAlertException {

		try {
			if (cIsDebug){
				cLogger.debug("TivoliAlertBussinessLogic handleEmailSenderEvent() method Start ::: ");
			}
			TivoliEmailSenderUtility temailsUtility = new TivoliEmailSenderUtility();
			prop = TivoliAlertProperties.getInstance();
			to = prop
					.getProperty(TivoliAlertConstants.MAIL_TO_DEV_TEAM);
			from = prop.getProperty(TivoliAlertConstants.MAIL_FROM_DEV_TEAM);
			cc = prop
					.getProperty(TivoliAlertConstants.MAIL_CC_DEV_TEAM);
			
			emailprinted = temailsUtility.sendEmail(to, cc, from, subject , pEmailTemplate);

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSINESSLOGIC,
					e.toString());
		}
		
		if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessLogic handleEmailSenderEvent() method End ::: ");
		}
		return emailprinted;
	}
	/**
	 * This method will check time difference between current date and last modified date of file-D:\\TivoliDeploy\\Logs\\TivoliLog.txt
	 * If their difference is greater than  tivoliUtilityLogTime, then java utility may be down so this method returns isEmailSend = true to send mail to DEV team
	 */
    public boolean handleUtilityStatus (String date1)  throws TivoliAlertException
    {
    	if (cIsDebug){
			cLogger.debug("TivoliAlertBussinessLogic handleUtilityStatus() method Start ::: ");
		}
    	 prop = TivoliAlertProperties.getInstance();
    	 int tivoliUtilityLogTime = Integer.parseInt(prop.getProperty(TivoliAlertConstants.tivoliUtilityLogTime));
    	 boolean isEmailSend = false;
    	 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
    	 try
    	 {
    		 Date startDate = formatter.parse(date1);
    		 Calendar previous = Calendar.getInstance();
    		 previous.setTime(startDate);
    		 Calendar now = Calendar.getInstance();
    		 long diff = now.getTimeInMillis() - previous.getTimeInMillis();
    		 if( diff > (tivoliUtilityLogTime * 60 * 1000))
    		 {
    			 //difference is greater than tivoliUtilityLogTime means status monitoring logfile is not updated on time, it may conclude that utility is down.
    			 //So sendmail to dev team
    			 isEmailSend = true;
    			 if (cIsDebug){
    					cLogger.debug("TivoliAlertBussinessLogic handleUtilityStatus() in isEmailSend: true ");
    				}
    		 }
    	 }
    	 catch (ParseException e)
    	 {
    		 cLogger.error(e.toString());
    		 throw new TivoliAlertException(
    				 TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSINESSLOGIC,
    				 e.toString());

    	 }catch (Exception e) {
    		 cLogger.error(e.toString());
    		 throw new TivoliAlertException(
    				 TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTBUSINESSLOGIC,
    				 e.toString());
    	 }
    	 if (cIsDebug){
 			cLogger.debug("TivoliAlertBussinessLogic handleUtilityStatus() method end ::: ");
 		}
        return isEmailSend;     
    }
}