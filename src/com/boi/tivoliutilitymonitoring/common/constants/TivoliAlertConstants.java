/*
 * TivoliAlertConstants.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */

package com.boi.tivoliutilitymonitoring.common.constants;

/**
 * This class will be having all constants which are used in Tivoli application.
 */
public final class TivoliAlertConstants {

	// To field for Email
	public static final String MAIL_TO_RECIPIENT_ADDRESS = "to";
	// CC field for Email
	public static final String MAIL_CC_RECIPIENT_ADDRESS = "cc";
	// From Field for Email
	public static final String MAIL_FROM_ADDRESS = "from";
	// Email Template
	public static final String TIVOLI_EMAIL_TEMPLATE = "emailtemplate";
	// Mail Server localhost
	public static final String localhost = "localhost";
	// Mail Server
	public static final String mailserver = "mailserver";
	
	// Configuration properiy file path
	public static final String TivoliAlertProperties = "config/tivolialertmonitoringconfig.properties";
	// Subject for Email
	public static final String TIVOLI_EMAIL_SUBJECT = "subject";
	// Root Logger
	public static final String TIVOLI_ROOT_LOGGER = "";
	
	// move file to fail folder
	public static final String failloc = "failloc";
	// log4j.properties path
	public static final String tivolimonitoring_logj = "logj";
	// errorlog constant variable
	public static final String error_log = "errorlog";
	// polling Interval constant variable
	public static final String folderMonitorInterval = "folderMonitorInterval";
	// To field for Email
	public static final String EMAIL_TO = "_email_to";
	// CC field for Email
	public static final String EMAIL_CC = "_email_cc";
	
	public static String TIVOLI_ALERT = "TIVOLI_ALERT";
	// tivoli java utitlity running status check log  file
	public static final String tivoliUtilityLog = "tivoliUtilityLog";
	
	public static final String tivoliUtilityLogTime = "tivoliUtilityLogTime";
	
	public static final String PROP_NULLLOGSYSTEM_CLASS = "org.apache.velocity.runtime.log.NullLogSystem";
	
	public static final String EMAIL_TEMPLATE_PATH = "emailtemplatepath";
	
	public static final String MONITORING_EMAIL_TEMPLATE_PATH = "monitoring_emailtemplatepath";
	
	public static final String MONITORING_TIVOLI_EMAIL_TEMPLATE = "monitoring_emailtemplate";
	
	public static final String failedFiles_email_msg = "failedFiles_email_msg";
	
	public static final String failedFiles_email_subject = "failedFiles_email_subject";

	public static final String utilityDown_email_msg = "utilityDown_email_msg";
	
	public static final String utilityDown_email_msg_email_subject = "utilityDown_email_subject";

	// To field for Email
	public static final String MAIL_TO_DEV_TEAM = "dev_to";
	// CC field for Email
	public static final String MAIL_CC_DEV_TEAM = "dev_cc";
	// From Field for Email
	public static final String MAIL_FROM_DEV_TEAM = "dev_from";
}