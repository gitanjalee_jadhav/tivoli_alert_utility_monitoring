/*
 * TivoliTemplateBuilder.java
 *
 * Copyright (C) 2014 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * disassembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.
 * 
 Modification History
 * Date	    	Version		  Author              Description
 * ----------	------- 	---------------     ------------------
 * May 30, 2018	  1.0       	OFSSL 	   			Created
 * 
 */

package com.boi.tivoliutilitymonitoring.common.util;

import java.io.StringWriter;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;

import com.boi.tivoliutilitymonitoring.common.constants.TivoliAlertConstants;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliErrorConstants;
import com.boi.tivoliutilitymonitoring.common.exception.TivoliAlertException;

/**
 * This  class is used to build email template for utility monitoring status mail send activity
 */
public class TivoliTemplateBuilder {
	
	private static Logger cLogger = Logger.getLogger(TivoliTemplateBuilder.class);
	public Boolean cIsDebug = false;
	private StringWriter swriter = null;


	public TivoliTemplateBuilder() {
		super();
		cIsDebug = cLogger.isDebugEnabled();
	}

	/**
	 * This  method is used to build email template for monitoring status mail send activity
	 */
	public StringWriter buildTemplate(String msgText)
	    throws TivoliAlertException
	{

		try {
			if (cIsDebug) {
				cLogger.debug("Alert's email template build in TivoliTemplateBuilder.handleTivoliEmailTemplateEvent() start::");
			}

			TivoliAlertProperties prop = TivoliAlertProperties.getInstance();
			
			Properties p = new Properties();   
	        p.setProperty("resource.loader","file");   
	        p.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.FileResourceLoader");   
	        p.setProperty("file.resource.loader.path", prop.getProperty(TivoliAlertConstants.MONITORING_EMAIL_TEMPLATE_PATH));              
	        p.setProperty("file.resource.loader.cache", "false");   
	        p.setProperty("file.resource.loader.modificationCheckInterval", "0");   

	        Velocity.init(p);      
	        Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, TivoliAlertConstants.PROP_NULLLOGSYSTEM_CLASS);
	        Template t = Velocity.getTemplate(prop.getProperty(TivoliAlertConstants.MONITORING_TIVOLI_EMAIL_TEMPLATE));
	        
	        VelocityContext context = new VelocityContext();

			context.put("emailstring", msgText);
			swriter = new StringWriter();
			t.merge(context, swriter);
			if (cIsDebug) {
				cLogger.debug("Alert's email template built successfully in TivoliTemplateBuilder.handleTivoliEmailTemplateEvent() End ::");
			}

		} catch (Exception e) {
			cLogger.error(e.toString());
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTTEMPLATEBUILDER,
					e.toString());
		}
		if (cIsDebug){
			cLogger.debug("TivoliTemplateBuilder handleInsertEvent() method End ::: ");
		}
		return swriter;
	}
}
