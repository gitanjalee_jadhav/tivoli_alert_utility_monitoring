/*
 * TivoliEmailSenderUtility.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoliutilitymonitoring.common.util;

import java.io.StringWriter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import com.boi.tivoliutilitymonitoring.common.constants.TivoliAlertConstants;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliErrorConstants;
import com.boi.tivoliutilitymonitoring.common.exception.TivoliAlertException;

/**
 * This class will be responsible for send the email.
 */
public class TivoliEmailSenderUtility {

	private boolean emailstatus = false;
	Properties properties = null;
	String host = null;
	TivoliAlertProperties prop = null;
	
	private static Logger cLogger = Logger.getLogger(TivoliEmailSenderUtility.class);
	public Boolean cIsDebug = false;

	public TivoliEmailSenderUtility() {
		cIsDebug = cLogger.isDebugEnabled();
	}

	/**
	 * This method will be responsible for send the email a per provided
	 * details.
	 */
	public boolean sendEmail(String to, String cc, String from,String subject,
			StringWriter emailstr) throws TivoliAlertException {
		try {
			if (cIsDebug){
				cLogger.debug("TivoliEmailSenderUtility sendEmail() method Start ::: ");
			}
			emailstatus = false;
			prop = TivoliAlertProperties.getInstance();
			host = prop.getProperty(TivoliAlertConstants.localhost);

			properties = System.getProperties();

			properties.setProperty(
					prop.getProperty(TivoliAlertConstants.mailserver), host);

			Session session = Session.getDefaultInstance(properties);

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			
			message.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(to, false));
			message.setRecipients(javax.mail.Message.RecipientType.CC, InternetAddress.parse(cc, false));
			if (cIsDebug){
				cLogger.debug("In  TivoliEmailSenderUtility :: In sendEmail()->TO address : "+to+ "  and CC address : "+cc);
			}
			message.setSubject(subject);
			message.setContent(emailstr.toString() ,"text/html");
			try {
				Transport.send(message);
				if (cIsDebug){
					cLogger.debug("In  TivoliEmailSenderUtility :: In sendEmail()->Mail sent successfully!!");
				}
			} catch (Exception e) {
				cLogger.error(e);
				throw new TivoliAlertException(
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEMAILSENDERUTILITY,
						TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEMAILSENDERUTILITY);
			}

			emailstatus = true;
			if (cIsDebug){
				cLogger.debug("TivoliEmailSenderUtility sendEmail() method End ::: ");
			}
		} catch (Exception e) {
			cLogger.error(e);
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTEMAILSENDERUTILITY,
					e.toString());
		}

		return emailstatus;
	}

}