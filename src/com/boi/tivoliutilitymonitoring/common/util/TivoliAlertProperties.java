/*
 * TivoliAlertProperties.java
 *
 * Copyright (C) 2015 by Bank of Ireland. All rights
 * reserved. Bank of Ireland. claims copyright in this
 * computer program as an unpublished work, one or more versions of
 * which were first used to provide services to customers on the dates
 * indicated in the foregoing notice. Claim of copyright does not imply
 * waiver of other rights.
 * NOTICE OF PROPRIETARY RIGHTS
 * This program is a confidential trade secret and the property of
 * Bank of Ireland. Use, examination, reproduction,
 * dis-assembly, decompiling, transfer and/or disclosure to others of all
 * or any part of this software program are strictly prohibited except
 * by express written agreement with Bank of Ireland.

Modification History

 * Date	    	 	Version		 Author              Description
 * ------------	 	------- 	 ---------------     ------------------
 * April 26, 2018	 1.0          OFSSL				 Created
 */
package com.boi.tivoliutilitymonitoring.common.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import com.boi.tivoliutilitymonitoring.common.constants.TivoliAlertConstants;
import com.boi.tivoliutilitymonitoring.common.constants.TivoliErrorConstants;
import com.boi.tivoliutilitymonitoring.common.exception.TivoliAlertException;

/**
 * This class is used to load tivolialertconfig.properties file and store the
 * key-value pair in a Property object. The property file should be on class
 * path where the Tivoli Alert Email application is running.
 */
public final class TivoliAlertProperties {

	private static TivoliAlertProperties cRefProperties = null;
	private static Properties prop = null;
	private static Object cLock = new Object();
	private static InputStream input = null;
	private static String newpKey = null;

	private TivoliAlertProperties() {
	}

	public static TivoliAlertProperties getInstance()
			throws TivoliAlertException {
		try {
			if (cRefProperties == null) {
				synchronized (cLock) {
					if (cRefProperties == null)
						cRefProperties = new TivoliAlertProperties();
				}
			}
			load(TivoliAlertConstants.TivoliAlertProperties);
			return cRefProperties;
		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTPROPERTIES,
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERT_PROPERTIES);
		}
	}


	/**
	 * This method  is used to load tivolialertconfig.properties file and store the
	 * key-value pair in a Property object. 
	 */
	private static void load(String pTivoliPropertiesPathAndName)
			throws TivoliAlertException {
		prop = new Properties();
		try {
			input = new FileInputStream(pTivoliPropertiesPathAndName);
			prop.load(input);

		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTPROPERTIES,
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERT_PROPERTIES);
		}

	}

	/**
	 * This class is used to retrieve tivolialertconfig.properties file values when key is provided.
	 */
	public String getProperty(String pKey) throws TivoliAlertException {
		try {

			newpKey = prop.getProperty(pKey);
		} catch (Exception e) {
			throw new TivoliAlertException(
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERTPROPERTIES,
					TivoliErrorConstants.TIVOLI_MSG_TIVOLIALERT_PROPERTIES);
		}
		return newpKey;
	}
}